# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "awards", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci", force: :cascade do |t|
    t.decimal "premium", precision: 8, scale: 2, null: false
    t.integer "premium_tipo", null: false, comment: "0 - Perdeu\n1 - Acumulado\n2 - Bingo\n3 - Linha\n4 - Diagonal\n5 - Quadrado"
    t.integer "card_id", null: false
    t.index ["card_id"], name: "fk_awards_cards1_idx"
  end

  create_table "cards", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "status", default: 0, null: false
    t.decimal "price", precision: 8, scale: 2, null: false
    t.decimal "premium", precision: 8, scale: 2, default: "0.0", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sortition_id", null: false
    t.integer "user_id", null: false
    t.index ["sortition_id"], name: "fk_cards_sortitions1_idx"
    t.index ["user_id"], name: "fk_cards_users_idx"
  end

  create_table "dozens", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "value", null: false
    t.integer "position", null: false
    t.integer "status", default: 0, null: false, comment: "0 - Aguardando\n1 - Sorteada"
    t.integer "card_id", null: false
    t.index ["card_id"], name: "fk_dozens_cards1_idx"
  end

  create_table "orders", id: :integer, default: nil, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.decimal "value", precision: 8, scale: 2, default: "0.0", null: false
    t.integer "status", default: 0, null: false
    t.integer "payment_service", null: false, comment: "1 - Boleto\n2 - Transferencia\n3 - Pagseguro\n4 - Bonus Code"
    t.integer "from_user_id", null: false
    t.integer "to_user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.integer "status", null: false
    t.integer "freq_minutes", default: 15, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id", null: false
    t.index ["user_id"], name: "fk_regions_users2_idx"
  end

  create_table "results", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "order", null: false
    t.integer "value", null: false
    t.integer "sortition_id", null: false
    t.index ["sortition_id"], name: "fk_dozens_copy1_sortitions1_idx"
  end

  create_table "sortitions", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.string "description"
    t.integer "region_id"
    t.integer "sort_type", default: 1, null: false, comment: "1 - Sorteio geral\\n2 - Sorteio regiao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "begin_at", null: false
    t.index ["region_id"], name: "fk_sortitions_regions1_idx"
  end

  create_table "users", id: :integer, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8mb4", force: :cascade do |t|
    t.integer "level", default: 2, null: false, comment: "1 - Admin\\n2 - Client  \\n3 - Cambista"
    t.string "name", null: false
    t.string "email", limit: 45, null: false
    t.string "password_digest", null: false
    t.decimal "credit", precision: 8, scale: 2, default: "1.0", null: false
    t.string "phone", limit: 45
    t.integer "status", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "awards", "cards", name: "fk_awards_cards1"
  add_foreign_key "cards", "sortitions", name: "fk_cards_sortitions1"
  add_foreign_key "cards", "users", name: "fk_cards_users"
  add_foreign_key "dozens", "cards", name: "fk_dozens_cards1", on_update: :cascade, on_delete: :cascade
  add_foreign_key "regions", "users", name: "fk_regions_users2"
  add_foreign_key "results", "sortitions", name: "fk_dozens_copy1_sortitions1"
  add_foreign_key "sortitions", "regions", name: "fk_sortitions_regions1"
end
