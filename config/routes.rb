Rails.application.routes.draw do

  #root
  root to:'home#index' 

  #
  # Api 1.0  
  #
  namespace :api1 do
    # Rotas de usuario
    post '/users/login', to: 'users#login'      
    post '/users/new', to: 'users#new'    
    put '/users/edit', to: 'users#edit'    
    put '/users/edit-password', to: 'users#edit_password'    
    post '/users/recover', to: 'users#recover'
    get '/users/show', to: 'users#show'
    delete '/users/destroy', to: 'users#destroy' 
    # Rotas cartelas   
    get '/cards', to: 'cards#index'          
    get '/cards/list', to: 'cards#list'        
    get '/cards/ranking', to: 'cards#ranking'          
    get '/cards/full', to: 'cards#full'          
    post '/cards/buy', to: 'cards#buy'  
    # Rotas sortitions
    get '/sortitions/next', to: 'sortitions#next'   
    get '/sortitions/incurse', to: 'sortitions#incurse'   
    # Rotas results
    post '/results', to: 'results#index' 
    post '/results/all', to: 'results#all' 
    # Rotas parametros
    get '/params/show', to: 'params#show'    
  end

  #
  # Painel admin 
  #
  namespace :admin do
    # reports
    root to: 'reports#index'        
    post '/reports', to: 'reports#index'
    # regions
    resources :regions 
    # users
    resources :users, :except => [:show]  
    get '/users/login', to: 'users#set_login'             
    post '/users/login', to: 'users#login'        
    get '/users/logout', to: 'users#logout'
    # clients          
    resources :clients, :except => [:show]  
  end

end