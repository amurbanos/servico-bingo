# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"  
# end   

# Learn more: http://github.com/javan/whenever
require 'mkmf'

app_dir     = File.expand_path(File.dirname(__FILE__)) + "/.."
ruby_binary = find_executable 'ruby'

set :output, "#{app_dir}/log/cron.log"    


every 30.minutes do  
  command "cd #{app_dir} && #{ruby_binary} bin/rake sortitions:create_sortitions"
end