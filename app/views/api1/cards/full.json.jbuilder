# json.merge! @user.attributes
# json.merge! @data 

if @data[:card]
  json.merge! @data[:card].attributes 
  json.dozens do |one_dozen|
    json.array! @data[:card].dozen, one_dozen.attributes
  end
else
  json.merge! @data
end
