# json.merge! @user.attributes
# json.merge! @data 

json.array! @data[:cards] do |one_card|
  json.merge! one_card.attributes
  user = Hash.new
  user[:id] = one_card.user.id
  user[:name] = one_card.user.name
  json.user user
  json.dozens do |one_dozen|
    json.array! one_card.dozen, one_dozen.attributes
  end
end    