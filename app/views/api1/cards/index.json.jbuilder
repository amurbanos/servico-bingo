# json.merge! @user.attributes
# json.merge! @data 

json.array! @data[:cards] do |one_card|
  json.merge! one_card.attributes  
  json.dozens do |one_dozen|
    json.array! one_card.dozen, one_dozen.attributes
  end
end    