class CardChecker

  # 01 02 03 04 05
  # 06 07 08 09 10
  # 11 12 13 14 15

  # attributes
  attr_accessor :sortition
  attr_accessor :premium_full

  #
  # SETA VENCEDORES ACUMULADO
  #
  def set_params(sortition_id)
    self.sortition    = Sortition.find_by_id(sortition_id)    
    self.premium_full = Param.find_by_name("premium_full").value.to_f      
  end


  #
  # SETA VENCEDORES ACUMULADO
  #
  def set_full_bingo_premiun()
    # busca ganhadores
    cards_full = Card
      .where("cards.status = ?", 0)
      .where("cards.sortition_id = ?", self.sortition.id)  
      .joins(:sortition)
        .where("sortitions.status = ?", 0)  
      .joins(:dozen)
        .where("dozens.status = ?", 1)
        .group('dozens.status')
        .having(
          'count(dozens.status) = ?',   
          Rails.configuration.bingo.qtd_dozen
        ) 

    # se existem ganhadores
    if cards_full.length > 0
      self.sortition.status = 1
      self.sortition.save
      # seta lista de  premios
      Dozen.transaction do
        cards_full.each do |card|
          award = Award.new   
          award.premium_tipo = 1
          award.premium      = (self.premium_full / cards_full.length)
          award.card_id      = card.id  
          award.save   
        end
      end
      # seta premio na cartela
      Card.transaction do
        cards_full.each do |card| 
          card.status   = 1  
          card.premium  += (self.premium_full / cards_full.length)
          card.save      
        end
      end
      # seta premio no credito do usuario
      Card.transaction do
        cards_full.each do |card| 
          user_tmp         = User.find_by_id(card.user_id)  
          user_tmp.credit  += (self.premium_full / cards_full.length)
          user_tmp.save      
        end
      end
    end

  end 


  #  
  #
  #
  def set_bingo_premiun()

  end   


end