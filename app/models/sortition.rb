class Sortition < ApplicationRecord

	has_many :card
	has_many :award	

  # attributes
  attr_accessor :num_users
  attr_accessor :num_cards
  attr_accessor :bingo_premium
  attr_accessor :line_premium
  attr_accessor :two_line_premium


  def num_users
    self.num_users = 
      self.card
      .select('DISTINCT user_id')  
      .count
  end


  def num_cards
    self.num_cards = 
      self.card
      .count
  end


  def bingo_premium
    premium_bingo_percent = Param.find_by_name("premium_bingo_percent").value.to_f
    bingo_premium = ( 
      self.card.sum(:price) *   
      (premium_bingo_percent / 100)  
    )
    #
    if bingo_premium < 1000
      bingo_premium = 1000  
    end
    self.bingo_premium = bingo_premium   
  end


  def line_premium
    premium_line_percent = Param.find_by_name("premium_line_percent").value.to_f
    line_premium = (
      self.card.sum(:price) *   
      (premium_line_percent / 100)  
    )
    #
    if line_premium < 50
      line_premium = 50
    end
    self.line_premium = line_premium
  end


  def two_line_premium
    premium_two_line_percent = Param.find_by_name("premium_line_percent").value.to_f
    two_line_premium = (
      self.card.sum(:price) *   
      (premium_two_line_percent / 100)  
    )
    #
    if two_line_premium < 100
      two_line_premium = 100
    end
    self.two_line_premium = two_line_premium
  end


end