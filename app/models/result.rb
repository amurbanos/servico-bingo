class Result < ApplicationRecord  

  # relations
  belongs_to :sortition

  before_save       :set_rand_dozen   
  after_save_commit :set_card_dozen 
  after_save_commit :set_card_premiun 

  # attributes
  attr_accessor :min_dozen
  attr_accessor :max_dozen


  def min_dozen
    self.min_dozen = Rails.configuration.bingo.min_dozen 
  end


  def max_dozen
    self.max_dozen = Rails.configuration.bingo.max_dozen 
  end


  def self.set_and_return(user, sortition_id, sequence)

    sortition_tmp = Sortition
      .where("id = ?", sortition_id)           
      .where("begin_at < ?", Time.now)           
      .first  

    result = self
      .where("sortition_id = ?", sortition_id)      
      .where("sequence = ?", sequence)
      .joins(:sortition).where(
        "sortitions.status = ?", 0 
      )      
      .first


    previous_result = true

    if sequence.to_i > 1
      previous_result = self
        .where("sortition_id = ?", sortition_id)        
        .where("sequence = ?", (sequence.to_i - 1))
        .joins(:sortition).where(
          "sortitions.status = ?", 0 
        )      
        .first
    end

    if sortition_tmp != nil
      if result == nil && previous_result != nil
        result = Result.new
        result.sequence = sequence
        result.sortition_id = sortition_id
        result.save
      end
    end

    returns = self
      .where("sortition_id = ?", sortition_id)
      .where("sequence <= ?", sequence)
      .order("sequence DESC")  
    return returns     
  end

  
  private
    #
    # GERA DEZENA ALEATORIA
    #
    def set_rand_dozen   
      rand_dozens = (self.min_dozen..self.max_dozen).to_a.shuffle
      sorted_dozens = 
        Result.where(
          "sortition_id = ?", self.sortition_id
        )
      sorted_dozens.each do |s_dozens|
        rand_dozens.delete(s_dozens.value)
      end
      self.value = rand_dozens.pop 
    end


    #
    # SETA DEZENA COMO SORTEADA
    #
    def set_card_dozen   
      dozens = 
        Dozen.select("dozens.*")        
          .where(
            card_id: Card.select("id")  
              .where(sortition_id: self.sortition_id)    
          )
          .where(
            "value = ?", self.value
          )  
      Dozen.transaction do
        dozens.each do |doz|
          doz.status = 1  
          doz.save    
        end
      end
    end


    #
    # SETA CARTELA PREMIADA
    #
    def set_card_premiun
      puts "===============CARD CHECk================================".green   
      @card_checker = CardChecker.new
      @card_checker.set_params(self.sortition_id)       
      @card_checker.set_full_bingo_premiun()         
      puts "===============CARD CHECk================================".green  
    end

end