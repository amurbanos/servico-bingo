class Card < ApplicationRecord

  # relations
  belongs_to :user
  belongs_to :sortition
  has_many :dozen , -> { order(position: :asc) }  

  after_create_commit :add_dozens, :remove_credit  

  # attributes
  attr_accessor :qtd_dozens
  attr_accessor :max_dozen  
  attr_accessor :min_dozen  

  def qtd_dozens
    self.qtd_dozens = Rails.configuration.bingo.qtd_dozen  
  end

  def min_dozen
    self.min_dozen = Rails.configuration.bingo.min_dozen  
  end

  def max_dozen
    self.max_dozen = Rails.configuration.bingo.max_dozen 
  end

  # validation
  validate :credit_must_postitive
  validate :sortition_must_future, :on => :create  

  def credit_must_postitive
    if self.user
      if self.price > self.user.credit
        errors.add(:price, "Crédito insuficiente")
      end
    end
  end

  def sortition_must_future
    if self.sortition
      if Time.now > self.sortition.begin_at  
        errors.add(:sortition_id, "Sorteio já aconteceu")
      end
    end
  end


  # 
  # ADITIONAL METHODS
  # 
  private

    # adiciona cada dezena
    def add_dozens
      rand_dozens = (self.min_dozen..self.max_dozen).to_a.shuffle
      rand_dozens = rand_dozens[0...self.qtd_dozens]  
      rand_dozens = rand_dozens.sort { |a, z| z <=> a }  
      rand_positions = (1..self.qtd_dozens).to_a.shuffle        
      dozens = []  
      self.qtd_dozens.times do |i|
        dozens << Dozen.new(
          :position   => rand_positions.pop,          
          :value      => rand_dozens.pop,   
          :card_id    => self.id,      
          :status     => 0
        )
      end
      Dozen.transaction do
        dozens.each(&:save)
      end
    end

    # remove credito do usuario
    def remove_credit
      self.user.credit -= self.price
      user.save
    end

end