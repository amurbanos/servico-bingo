class Admin::ApplicationController < ActionController::Base    

  before_action :authorize_request
  skip_before_action :verify_authenticity_token

  layout "admin"


  def authorize_request
    begin
      @decoded = JsonWebToken.decode(session[:current_user_token])      
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound => e
      redirect_to controller: "users", action: "login"
    rescue JWT::DecodeError => e
      redirect_to controller: "users", action: "login"    
    end
  end


end
