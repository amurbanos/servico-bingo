class Admin::UsersController < Admin::ApplicationController

  before_action :authorize_request, except: [:login, :set_login]  
  before_action :set_admin_user, only: [:show, :edit, :update, :destroy]


  def set_login
    render :login, layout: "admin_login"
  end


  def login
    user = User
      .where("email"  => params[:email])      
      .where("status" => 1)     
      .where("level" => 1)     
      .first 
    @data = Hash.new
    if user&.authenticate(params[:password])    
      ap user
      session[:current_user_token] = JsonWebToken.encode(user_id: user.id) 
      redirect_to '/admin/users'
    else
      render :login, layout: "admin_login"   
    end  
  end


  def logout
    session[:current_user_token] = nil  
    redirect_to controller: "users", action: "login"    
  end


  def index
    @admin_users = User.where("level" => 1)
  end 


  def show
  end


  def new
    @admin_user = User.new
  end


  def edit
  end


  def create
    @admin_user = User.new(admin_user_params)

    if @admin_user.save
      redirect_to @admin_user, notice: 'User was successfully created.'
    else
      render :new
    end
  end


  def update
    if @admin_user.update(admin_user_params)
      redirect_to @admin_user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end


  def destroy
    @admin_user.destroy
    redirect_to admin_users_url, notice: 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_user
      @admin_user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_user_params
      params.require(:admin_user).permit(:level, :name, :email, :password, :credit, :credit, :phone)
    end
end
