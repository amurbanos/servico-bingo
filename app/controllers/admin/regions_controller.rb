class Admin::RegionsController < Admin::ApplicationController
  before_action :set_admin_region, only: [:show, :edit, :update, :destroy]

  # GET /admin/regions
  def index
    @admin_regions = Region.all
  end

  # GET /admin/regions/1
  def show
  end

  # GET /admin/regions/new
  def new
    @admin_region = Region.new
  end

  # GET /admin/regions/1/edit
  def edit
  end

  # POST /admin/regions
  def create
    @admin_region = Region.new(admin_region_params)

    if @admin_region.save
      redirect_to @admin_region, notice: 'Region was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /admin/regions/1
  def update
    if @admin_region.update(admin_region_params)
      redirect_to @admin_region, notice: 'Region was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /admin/regions/1
  def destroy
    @admin_region.destroy
    redirect_to admin_regions_url, notice: 'Region was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_region
      @admin_region = Region.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def admin_region_params
      params.require(:admin_region).permit(:name, :user_id)
    end
end
