class Api1::CardsController < ApplicationController


  before_action :authorize_request, except: [:ranking, :full]  


  # 
  # THIS METHOD LIST ACTIVE CARDS 
  # 
  def index
    cards = Card
      .where("user_id = ?", @current_user.id)      
      .where("status = ?", 0)      
      # .joins(:sortition).where("begin_at > ?", Time.now)    
    @data = Hash.new
    if cards    
      @data[:cards] = cards         
    else
      @data[:cards] = nil
    end
  end


  # 
  # THIS METHOD LIST RANKING CARDS 
  # 
  def ranking
    cards = Card
      .joins(:sortition)
        .where("sortitions.begin_at > ?", Time.now)
      .joins(:user)  
      .order('RAND()')
      .limit(10)   
    @data = Hash.new
    if cards    
      @data[:cards] = cards       
    else
      @data[:cards] = nil
    end
  end


  # 
  # THIS METHOD RETURN FULL CARD 
  # 
  def full
    cards = Card
      .joins(:sortition).where("sortitions.begin_at > ?", Time.now)
      .joins(:dozen)
        .where("dozens.status = ?", 1)
        .group('dozens.card_id')
        .having('count(dozens.card_id) = ?', Rails.configuration.bingo.qtd_dozen)
      .first 
    @data = Hash.new
    if cards    
      @data[:card]   = cards      
    end
  end


  # 
  # THIS METHOD LIST USER CARDS 
  # 
  def list
    cards = Card
      .where("user_id = ?", @current_user.id)
      .where("status <> ?", 3)
      .order('status DESC, id DESC') 
      .limit(1000)      
    @data = Hash.new  
    if cards    
      @data[:cards] = cards          
    else
      @data[:cards] = nil
    end
  end


  # 
  # THIS METHOD SET NEW CARD 
  # 
  def buy
    qtd_cards = params[:qtd_cards].to_i
    @data     = Hash.new

    #salva cada cartela
    qtd_cards.times do |i|    
      @card = Card.new
      @card.user_id       = params[:user_id]    
      @card.user_id       = @current_user.id      
      @card.price         = params[:price]    
      @card.sortition_id  = params[:sortition_id]
      if @card.save  
         @data[:card_save_status] = 1        
      else
         @data[:card_save_status] = 0 
         @data[:errors] = @card.errors 
      end         
    end  
  end


  # 
  # THIS METHOD DESTROY A CARD 
  # 
  def destroy
    @card = Card.find_by id: params[:id]   
    @data = Hash.new
    @data[:card_delete_status] = 0 
    if @card
      if @card.destroy  
        @data[:card_delete_status] = 1    
      end
    end    
  end


end