class Api1::UsersController < ApplicationController


  before_action :authorize_request, except: [:login, :new]


  # 
  # THIS METHOD SET LOGIN 
  #   
  def login
    user = User
      .where("email"  => params[:user])      
      .where("status" => 1)     
      .where("level" => 2)     
      .first 
    @data = Hash.new
    if user&.authenticate(params[:password])
      # user = user.attributes.except('password_digest')   
      @data[:token]           = JsonWebToken.encode(user_id: user.id)
      @data[:time]            = Time.now + 24.hours.to_i
      @data[:login_status]    = 1
      @data[:login_message]   = "Logado com sucesso!!!"    
      @data[:user] = user
    else
      @data[:login_status]    = 0  
      @data[:login_message]   = "Usuário ou senha inválidos..."  
      @data[:user] = nil
    end   
  end


  # 
  # THIS METHOD UPDATE USER 
  #   
  def edit
    @user = User
      .where("id"  => @current_user.id)      
      .first 
    @user.name     = params[:name]    
    @user.email    = params[:email]    
    @user.phone    = params[:phone]     
    @data = Hash.new 
    if @user.save
       @data[:user_save_status] = 1
       @data[:user] = @user
    else
       @data[:user] = @user
       @data[:user_save_status] = 0
       @data[:errors] = @user.errors
    end   
  end


  # 
  # THIS METHOD UPDATE PASSWORD 
  #   
  def edit_password
    @user = User
      .where("id"  => @current_user.id)      
      .first 
    @user.password = params[:password]    
    @data = Hash.new 
    if @user.save
       @data[:user_save_status] = 1  
       @data[:user] = @user
    else
       @data[:user] = @user
       @data[:user_save_status] = 0
       @data[:errors] = @user.errors
    end       
  end


  # 
  # THIS METHOD SET NEW A USER
  # 
  def new
    @user = User.new
    @user.name     = params[:name]    
    @user.email    = params[:email]    
    @user.phone    = params[:phone]    
    @user.password = params[:password]  
    @data = Hash.new 
    if @user.save
       @data[:user_save_status] = 1
       @data[:user] = @user
    else
       @data[:user] = nil
       @data[:user_save_status] = 0
       @data[:errors] = @user.errors
    end  
  end


  # 
  # THIS METHOD GET LOGGED USER  
  # 
  def show
    @data = Hash.new  
    @data[:user] = @current_user
  end


  # 
  # This method set new a user 
  # 
  def load
    @users = User.all
    render json: @users, status: :ok    
  end


  # 
  # THIS METHOD DISABLE A USER 
  # 
  def destroy
    user = User.find_by id: @current_user.id   
    @data = Hash.new
    @data[:user_delete_status] = 0   
    if user
      user.status = 0  
      if user.save  
        @data[:user_delete_status] = 1    
      end
    end    
  end

end