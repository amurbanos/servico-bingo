class Api1::SortitionsController < ApplicationController


  before_action :authorize_request, except: [:next]  


  # 
  # THIS METHOD LIST LAST SORTITION 
  # 
  def next
    sortition = Sortition
      .select(
        "sortitions.*"
      )
      .where("begin_at > ?", Time.now)    
      .first
    num_users =     

    @data = Hash.new 
    if sortition   
       @data[:sortition]          = sortition  
       @data[:num_users]          = sortition.num_users  
       @data[:num_cards]          = sortition.num_cards  
       @data[:one_line_premium]   = sortition.line_premium  
       @data[:two_line_premium]   = sortition.line_premium  
       @data[:bingo_premium]      = sortition.bingo_premium  
    else
       @data[:sortition] = nil
    end  
  end

  # 
  # THIS METHOD LIST In CURSE SORTITION 
  # 
  def incurse
    sortition = Sortition
      .select(
        "sortitions.*"
      )
      .where("begin_at < ?", Time.now)    
      .where("status = ?", 0)    
      .first
    num_users =     

    @data = Hash.new 
    if sortition   
       @data[:sortition]          = sortition  
       @data[:num_users]          = sortition.num_users  
       @data[:num_cards]          = sortition.num_cards  
       @data[:one_line_premium]   = sortition.line_premium  
       @data[:two_line_premium]   = sortition.two_line_premium  
       @data[:bingo_premium]      = sortition.bingo_premium  
    else
       @data[:sortition] = nil
    end  
  end


  # 
  # THIS METHOD GET DOZENS
  # 
  def get_dozens
      
  end


end