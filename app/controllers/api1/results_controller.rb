class Api1::ResultsController < ApplicationController

  # 
  # THIS METHOD GET DOZENS
  # 
  def index
    limit = 4
    if params[:limit]
      limit = params[:limit]
    end
    @data = Hash.new
    @data[:results] = Result.set_and_return(
    	@current_user,
    	params[:sortition_id], 
    	params[:sequence]
    )
  end

  # 
  # THIS METHOD GET DOZENS
  # 
  def all
    all_dozens  = []
    99.times do |i|
      all_dozens[i] = Hash.new
      all_dozens[i][:value]  = i + 1 
      all_dozens[i][:status] = 0
      result_tmp = Result
        .where("sortition_id = ?", params[:sortition_id])      
        .where("value = ?", all_dozens[i][:value]).first
      if result_tmp
        all_dozens[i][:status] = 1 
      end     

    end
    @data = Hash.new
    @data[:all_dozens] = all_dozens
  end

end