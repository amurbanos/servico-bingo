namespace :sortitions do

  desc "Criar sorteios"
  task create_sortitions: :environment do
    date_now = DateTime.now + ( 2/24.0 )    
    regions  = Region.all

    sortitions = []  
    regions.each do |region|
      sortitions << Sortition.new(
        :description  => "Sorteio",     
        :region_id    => region.id, 
        :sort_type    => 2,  
        :begin_at     => date_now  
      )

    end
    sortitions.each(&:save)  
  end


  desc "teste de criar carte"
  task create_card: :environment do
    card = Card.new(
      :price        => 0.70, 
      :user_id      => 7,
      :sortition_id => 79
    )
    card.save 
    puts card.errors.inspect    
  end


end
